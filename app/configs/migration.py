from flask_migrate import Migrate
from flask import Flask


def init_app(app: Flask):
    from app.models.leads_model import LeadModel

    Migrate(app, app.db)
    