from sqlalchemy.orm import defaultload
from app.exc.errors import ChangeKeys, TelephoneNumber, EmailType
from datetime import datetime
from re import T
import re
from sqlalchemy.sql.expression import false
from app.configs.database import db
from dataclasses import dataclass
from sqlalchemy.sql.sqltypes import DateTime, Integer
from sqlalchemy import Column, String
from app.configs.database import db

@dataclass
class LeadModel(db.Model):
    name: str
    email: str
    phone: str
    creation_date: DateTime
    last_visit: DateTime
    visits: int

    __tablename__ = 'lead_card'

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    email = Column(String,unique=True, nullable=False)
    phone = Column(String,unique=True, nullable=False)
    creation_date = Column(DateTime, nullable=True, default=datetime.today)
    last_visit = Column(DateTime, nullable=True, default=datetime.today)
    visits = Column(Integer, nullable=True, default=1)

    @staticmethod
    def check_telephone(data):
        print(data['phone'], '*****')
        if re.fullmatch('\(\d{2}\)\d{5}\-\d{4}', data['phone']) == None:
            raise TelephoneNumber('Your cellphone must be in this format (xx)xxxxx-xxxx')

    @staticmethod
    def right_keys(data):
        for key in data.keys():
            if key not in data:
                raise ChangeKeys('Your request must has only name, email and phone')

    @staticmethod
    def check_email(data):
        if type(data['email']) != String:
            raise EmailType('Your email must be a String')

    def update_visits(self):
       self.visits = self.visits + 1

    
    def update_last_visit(self):
        self.last_visit = datetime.today()
