from app.controllers.lead_controller import create_leads, delete_lead, get_leads
from flask import Blueprint
from app.controllers.lead_controller import create_leads, get_leads, new_visit

bp = Blueprint('leads', __name__, url_prefix='/lead')

bp.post('')(create_leads)
bp.get('')(get_leads)
bp.patch('')(new_visit)
bp.delete('')(delete_lead)