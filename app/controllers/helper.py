from app.exc.errors import  EmptyArray

def no_content(data):
        if not data:
            raise EmptyArray('Your database has no leads')