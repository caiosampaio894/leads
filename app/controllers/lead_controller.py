from dataclasses import dataclass
from datetime import datetime
import re
from sqlalchemy.sql.functions import user
from app.controllers.helper  import no_content
from app.exc.errors import  ChangeKeys, EmptyArray, TelephoneNumber
from flask import json, jsonify, request, current_app
from app.models.leads_model import LeadModel
from sqlalchemy.exc import IntegrityError, ProgrammingError, NoResultFound
def create_leads():
    data = request.json
    try:
        user_lead = LeadModel(**data)
        LeadModel.check_telephone(data)
        LeadModel.right_keys(data)

        session = current_app.db.session
        session.add(user_lead)
        session.commit()

        return jsonify(user_lead), 201

    except IntegrityError as e:
        return {'message': str(e.orig).split('\n')[0]}, 400
    
    except TelephoneNumber as e:
        return {'message': str(e)}, 400
    
    except TypeError as e:
        return {'message': str(e)}
    
def get_leads():
    try:
        user_lead_list = LeadModel.query.order_by(LeadModel.visits.desc()).all()
        no_content(user_lead_list)
        return jsonify(user_lead_list), 200

    except EmptyArray as e:
        return {'message': str(e)}, 404

def new_visit():
    data = request.get_json()
    try:
        query = LeadModel.query.filter_by(email=data['email']).one()
        LeadModel.update_last_visit(query)
        LeadModel.update_visits(query)
        session = current_app.db.session
        session.commit()
    
        return '', 204

    except ProgrammingError as e:
        return {'message': str(e.orig).split('\n')[0]}, 400
    
    except NoResultFound as e:
        return {'message': str(e)}, 404

def delete_lead():
    try:
        delete_lead_from_email = LeadModel.query.filter_by(email=request.json['email']).one()
        session = current_app.db.session
        session.delete(delete_lead_from_email)
        session.commit()

        return jsonify(delete_lead_from_email), 204

    except ProgrammingError as e:
        return {'message': str(e.orig).split('\n')[0]}, 400
    
    except NoResultFound as e:
        return {'message': str(e)}, 404
